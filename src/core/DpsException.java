package core;

public class DpsException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3515979466928881340L;

	public DpsException(String s){
		super(s);
	}
	
}
