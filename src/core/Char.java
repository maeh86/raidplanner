package core;

public class Char{
  
	  public enum Klasse{
		  PALADIN, PRIESTER, HEXENMEISTER, SCHURKE, TODESRITTER, MAGIER, DRUIDE, KRIEGER, JAEGER, SCHAMANE, MOENCH
	  }
	  
	  public enum Specc{
		  TANK, HEALER, RANGED, MELEE, EMPTY
	  }
  
  private Player player;
  private String name;
  private Specc mainSpecc;
  private Specc secSpecc;
  private Klasse eK; //enum Klasse 
  

  
  public Char(Player p, String n, Klasse c, Specc s1, Specc s2){
    
    setName(n);
    setCharClass(c);
    setMainSpecc(s1);
    setSecSpecc(s2);
    setPlayer(p);
    }
  
  public boolean setPlayer(Player p){
    if(p == null) return false;
    player = p;
    p.addChar(this);
    return true;
  }
  
  public boolean setName(String n){
    if(n == null) return false;
    name = n;
    return true;
  }
    
  public boolean setMainSpecc(Specc s){
    if(s == null) return false;
    mainSpecc = s;
    return true;
  }
  
  public boolean setSecSpecc(Specc s){
    if(s == null) return false;
    secSpecc = s;
    return true;
  }
  
  public Player getPlayer(){
    return player;  
  }
  
  public String getName(){
    return name;
  }
   
  public Specc getMainSpecc(){
    return mainSpecc;
  }
  
  public Specc getSecSpecc(){
    return secSpecc;
  }
  
  public boolean setCharClass(Klasse c){
	  if (c == null) return false;
	  eK = c;
	  return true;
  }
  
  public Klasse getCharClass(){
	  return eK;
  }
  
  public String toString(){
	  //TODO: hier evtl. noch die Klasse/Specc ausgeben f�r die UI
	  return name;
  }
  
  public boolean isTank(){
	  if((getMainSpecc() == Specc.TANK)||(getSecSpecc() == Specc.TANK)){
		  return true;
	  }
	  return false;
  }
  
  public boolean isHealer(){
	  if((getMainSpecc() == Specc.HEALER)||(getSecSpecc() == Specc.HEALER)){
		  return true;
	  }
	  return false;
  }
  
  public boolean isRanged(){
	  if((getMainSpecc() == Specc.RANGED)||(getSecSpecc() == Specc.RANGED)){
		  return true;
	  }
	  return false;
  }
  
  public boolean isMelee(){
	  if((getMainSpecc() == Specc.MELEE)||(getSecSpecc() == Specc.MELEE)){
		  return true;
	  }
	  return false;
  }
  
}
