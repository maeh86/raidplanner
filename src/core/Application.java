package core;

import java.awt.Dimension;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JFrame;

public class Application extends JFrame{
  /**
	 * 
	 */
	private static final long serialVersionUID = -4655804731019105830L;

private static Application instance;
    
    private List<Raid> raidList;
    private List<Player> playerList;
    
    private Application(){
      Init();
    }
  
  public static Application getInstance(){
    if (instance == null) {
      instance = new Application();
    }
    return instance;
  }
  
  private void Init(){
    raidList = new ArrayList<Raid>();
    playerList = new ArrayList<Player>();
    
    // UI Initialisieren
    setVisible(true);
    setSize(new Dimension(1024, 768));
    setTitle("Dead Poets Society RaidPlanner");
  }
  
  /** Adapter für Init(), setzt das Programm zurück **/
  public void resetApp(){
    Init();
  }
  
  /* fügt Player p der playerList hinzu -> mit Prüfung, ob bereits vorhanden*/
  public boolean addPlayer(Player p){
    if(playerList.contains(p)){
    	System.out.println("Spieler " + p.getName() + " ist bereits vorhanden.");
      return false;
    }else{
      playerList.add(p);
      System.out.println("Spieler " + p.getName() + " wurde erfolgreich hinzugef�gt.");
      return true;
    }
  }
  
  /* fügt Raid r der raidList hinzu -> mit Prüfung, ob bereits vorhanden*/
  public boolean addRaid(Raid r){
    if(raidList.contains(r)){
      return false;
    }else{
      raidList.add(r);
      return true;
    }
  }
  
  public void clearPlayerList(){
    playerList = new ArrayList<Player>();
  }
  
  public void clearRaidList(){
    raidList = new ArrayList<Raid>();
  }
  
  /*Startet die Planung*/
  public Raidplanner plan(){
    if((raidList == null)||(playerList == null)){
      return null;
    }else{
      return new Raidplanner(playerList);  
    }
    
  }
  
}
