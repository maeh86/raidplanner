package core;

import java.util.ArrayList;
import java.util.List;

public class Player{
  
  private String name;
  private List<Char> chars;
  private Char mainChar;
  private int playerID = -1;
  
  public Player(String n, List<Char> c, int id){
    if (n != null) name = n; else name = "";
    if (c != null) chars = c; else chars = new ArrayList<Char>();
    mainChar = null; //TODO
    try {
    	if (id<1) throw new DpsException("PlayerID f�r " + name + " -> ung�ltig");
        playerID = id;	
    } catch (DpsException e) {
    	System.out.println(e.getMessage());
    	System.exit(1);
    }
    
  }
  
  public Player(String n, int id){
	  this(n, null, id);
  }
  
  public boolean addChar(Char c){
    if (chars == null) return false;
    if (c == null) return false;
    else {
      setMainChar(c);
      boolean temp = chars.add(c);
      if (temp == false) System.out.println("Fehler bei der Charakterzuweisung: " + this.getName() + " -> " + c.getName());
      else System.out.println("Charakterzuweisung erfolgreich: " + this.getName() + " -> " + c.getName());
      return temp;
    }
  }
  
  public boolean removeChar(Char c){
    if(chars == null) return false;
    if(c == null) return false;
    else {
      if(chars.size() == 1){
        mainChar = null;
      }
      return chars.remove(c);
    }
  }
  
  public List<Char> getChars(){
    return chars;
  }
  
  public boolean setMainChar(Char c){
    if (mainChar == null) return false;
    if (c == null) return false;
    else {
      mainChar = c;
      return true;
    }
  }
  
  public String getName(){
	return name;  
  }
  
  
  public boolean setName(String n){
	  if(n == null) return false;
	  name = n;
	  return true;
  }
  
  public Char hasTankChar(){
	  for (Char c : chars){
		  if (c.isTank()) return c;
	  }
	  return null;
  }
  
  public Char hasHealerChar(){
	  for (Char c : chars){
		  if (c.isHealer()) return c;
	  }
	  return null;
  }
}
