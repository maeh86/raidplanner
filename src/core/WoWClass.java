package core;

public abstract class WoWClass {

  private String name;

  public WoWClass(){
    initClassName();
  }

  public String getName(){
    return name;
  }

 public boolean setName(String n){
  if(n == null) return false;
  name = n;
  return true;
 }
 
 abstract void initClassName();
 
}
