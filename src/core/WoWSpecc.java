package core;

public abstract class WoWSpecc {

	public abstract boolean isTank();
	public abstract boolean isHealer();
	public abstract boolean isRanged();
	public abstract boolean isMelee();
	
}
