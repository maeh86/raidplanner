package core;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Raidplanner {
	
	private List<Player> playerList;
	private List<Player> plannedPlayersList;
	
	private Raid raid1;
	private Raid raid2;
	
	
	public Raidplanner(){
		playerList = new ArrayList<Player>();
		plannedPlayersList = new ArrayList<Player>();
		raid1 = new Raid(1);
		raid2 = new Raid(2);
	}
	
	public Raidplanner(List<Player> pL){
		this();
		if(pL != null) playerList = pL;
		useDefaultStrategy();
	}
	
	
	private void useDefaultStrategy() {
		//die anwendungslogik muss hier rein
		int minTanks = 2;
		int maxTanks = 2;
		int minHealer = (playerList.size()/5);
		int maxHealer = (playerList.size()/5) + 2;
		int minRanged = (playerList.size()/4);
		int maxMelee = (playerList.size()/3);
		
		
		//berechnung tanks
		Iterator<Player> it = playerList.iterator();
		List<Player> playersToRemove = new ArrayList<Player>();
		while (it.hasNext()){
			Player mayBeTank = it.next();
			Char c = mayBeTank.hasTankChar(); 
			if (c != null){
				raid1.addCharToRaid(c);
				playersToRemove.add(mayBeTank);
				plannedPlayersList.add(mayBeTank);
			}
			if (raid1.getTankIndex() > minTanks -1){
				break;
			}
		}
		playerList.removeAll(playersToRemove);
		
		playersToRemove.clear();
		
		//berechnung heiler
		//TODO: weiter
		it = playerList.iterator();
		while (it.hasNext()){
			Player mayBeHealer = it.next();
			Char c = mayBeHealer.hasHealerChar(); 
			if (c != null){
				raid1.addCharToRaid(c);
				playersToRemove.add(mayBeHealer);
				plannedPlayersList.add(mayBeHealer);
			}
			if (raid1.getTankIndex() > minTanks -1){
				break;
			}
		}
		playerList.removeAll(playersToRemove);
		
		playersToRemove.clear();
		
	}

	
	
	public Raid getRaid(int i){
		switch (i) {
		case 1: return raid1;
		case 2: return raid2;
		default: return null;
		}
	}
	
	
	
}
