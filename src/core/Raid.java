package core;

import java.util.ArrayList;
import java.util.List;

import javax.swing.JList;

import core.Char.Specc;

public class Raid extends JList<Char>{

	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 7430930655551870839L;

	private int iTank = 0;
	private int iHealer = 0;
	private int iRanged = 0;
	private int iMelee = 0;
	
	private List<Char> charList;
	private int raidID = -1;
	
	

	public Raid(){
		charList = new ArrayList<Char>();
	}
	
	public Raid(int i){
		this();
		raidID = i;
	}
	
	public int getTankIndex(){
		return iTank;
	}
	
	public int getHealerIndex(){
		return iHealer;
	}
	
	public int getRangedIndex(){
		return iRanged;
	}
	
	public int getMeleeIndex(){
		return iMelee;
	}
	
	public int getRaidID() {
		return raidID;
	}

	public void setRaidID(int raidID) {
		this.raidID = raidID;
	}
	
	public void updateIndex(){
		calcTankIndex();
		calcHealerIndex();
		calcRangedIndex();
		calcMeleeIndex();
		System.out.println("Indexneuberechnung: " + "T->" + getTankIndex() + " | H->" + getHealerIndex() + " | R->" + getRangedIndex() + " | M->" + getMeleeIndex());
		
	}
	
	private void calcTankIndex(){
		int i = 0;
		for(Char c : charList){
			if((c.getMainSpecc() == Specc.TANK)||(c.getSecSpecc() == Specc.TANK)) i++;
		}
		iTank = i;
	}
	
	private void calcHealerIndex(){
		int i = 0;
		for(Char c : charList){
			if((c.getMainSpecc() == Specc.HEALER)||(c.getSecSpecc() == Specc.HEALER)) i++;
		}
		iHealer = i;
	}
	
	private void calcRangedIndex(){
		int i = 0;
		for(Char c : charList){
			if((c.getMainSpecc() == Specc.RANGED)||(c.getSecSpecc() == Specc.RANGED)) i++;
		}
		iRanged = i;
	}
	
	private void calcMeleeIndex(){
		int i = 0;
		for(Char c : charList){
			if((c.getMainSpecc() == Specc.MELEE)||(c.getSecSpecc() == Specc.MELEE)) i++;
		}
		iMelee = i;
	}
	
	public boolean addCharToRaid(Char c){
		if(c == null) return false;
		else {		
			boolean temp = charList.add(c);
			updateIndex();
			if(temp == true) System.out.println("Char " + c.getName() + " wurde Raid " + raidID + " hinzugefügt." );
			else System.out.println("Fehler beim Hinzufügen von Char " + c.getName() + " in Raid " + raidID + "." );
			return temp;
		}
		//TODO: updateUI
	}
	
	public boolean removeCharFromRaid(Char c){
		if(c == null) return false;
		else {
			if (charList.contains(c)) return charList.remove(c);
			return false;
		} 
		//TODO: updateUI
	}

	public void consoleOutput() {
		String output = "";
		for (Char c : charList){
			output += c.toString() + " - "; 
		}
		System.out.println("Raid " + raidID + ": " + output);
	}
	
	
}
