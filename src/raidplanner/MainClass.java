/**
 * 
 */
package raidplanner;

import core.Application;
import core.Char;
import core.Char.Klasse;
import core.Char.Specc;
import core.Player;
import core.Raidplanner;

/**
 * @author Marc
 *
 */
public class MainClass {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Application app = Application.getInstance();
		
		Player p1 = new Player("Flo", 1);
		Char p1c1 = new Char(p1, "Foxxy", Klasse.KRIEGER, Specc.TANK, Specc.MELEE);
		Char p1c2 = new Char(p1, "Nebuchad", Klasse.TODESRITTER, Specc.TANK, Specc.EMPTY);
		Player p2 = new Player("Marc", 2);
		Char p2c1 = new Char(p2, "Crafty", Klasse.MOENCH, Specc.TANK, Specc.MELEE);
		Char p2c2 = new Char(p2, "Thoko", Klasse.PALADIN, Specc.TANK, Specc.HEALER);
		app.addPlayer(p1);
		app.addPlayer(p2);
		
		Raidplanner plan = app.plan();
		plan.getRaid(1).consoleOutput();
		plan.getRaid(2).consoleOutput();
		
	}

}
